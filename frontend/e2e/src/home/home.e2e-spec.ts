import { browser, by, element, logging } from 'protractor';
import { protractor } from 'protractor/built/ptor';
import { AppPage } from '../app.po';

describe('Home page ', () => {
  let page: AppPage;

  beforeEach(() => {
    page = new AppPage();
  });

  it('check input bottle x', async () => {
    let input: any = element(by.id('input-text-bottle-x'));
    return expect(input.isDisplayed()).toBe(true);
  });

  it('check input bottle y', async () => {
    let input: any = element(by.id('input-text-bottle-y'));
    return expect(input.isDisplayed()).toBe(true);
  });

  it('check input bottle z', async () => {
    let input: any = element(by.id('input-text-bottle-z'));
    return expect(input.isDisplayed()).toBe(true);
  });

  it('check bottle x', async () => {
    let input: any = element(by.id('bottle-x'));
    return expect(input.isDisplayed()).toBe(true);
  });

  it('check bottle y', async () => {
    let input: any = element(by.id('bottle-y'));
    return expect(input.isDisplayed()).toBe(true);
  });

  it('check bottle z', async () => {
    let input: any = element(by.id('bottle-z'));
    return expect(input.isDisplayed()).toBe(true);
  });

  it('calculate 5-3-4', async () => {
    let button: any = element(by.id('button-calculate'));
    browser.wait(protractor.ExpectedConditions.elementToBeClickable(button), 10000)
    .then ( function () {
      button.click();
    });
  });


  afterEach(async () => {
    // Assert that there are no errors emitted from the browser
    const logs = await browser.manage().logs().get(logging.Type.BROWSER);
    expect(logs).not.toContain(jasmine.objectContaining({
      level: logging.Level.SEVERE,
    } as logging.Entry));
  });
});
