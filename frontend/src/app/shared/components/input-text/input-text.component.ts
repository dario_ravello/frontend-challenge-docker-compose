import { Component, EventEmitter, Input, OnInit, Output } from '@angular/core';

@Component({
  selector: 'app-input-text',
  templateUrl: './input-text.component.html',
  styleUrls: ['./input-text.component.scss']
})
export class InputTextComponent implements OnInit {

  @Input()
  inputType!: string;

  @Input()
  inputLabel: string | undefined;

  @Input()
  inputLeftHint: string | undefined;

  @Input()
  inputRightHint: string | undefined;

  @Input()
  inputMaxLenght: number | undefined;

  @Input()
  inputPlaceholder: string | undefined;

  @Input()
  inputValue!: string;

  @Output()
  inputValueChange: EventEmitter<string> = new EventEmitter<string>();

  constructor() { }

  ngOnInit(): void {
  }

}
