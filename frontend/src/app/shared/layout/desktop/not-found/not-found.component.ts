import { Component, OnInit } from '@angular/core';
import { faGhost } from '@fortawesome/free-solid-svg-icons';

@Component({
  templateUrl: './not-found.component.html',
  styleUrls: ['./not-found.component.scss']
})
export class NotFoundComponent implements OnInit {

  faGhost = faGhost;

  constructor() { }

  ngOnInit(): void {
  }

}
