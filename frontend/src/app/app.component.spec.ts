import { TestBed, tick } from '@angular/core/testing';
import { RouterTestingModule } from '@angular/router/testing';
import { AppComponent } from './app.component';
import { LoadingService } from './core/services/loading.service';

describe('AppComponent', () => {
  beforeEach(async () => {
    await TestBed.configureTestingModule({
      imports: [
        RouterTestingModule
      ],
      declarations: [
        AppComponent
      ],
      providers: [LoadingService]
    }).compileComponents();
  });

  it('should create the app', () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });

  it(`should have as title 'frontend-challenge'`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    const app = fixture.componentInstance;
    expect(app.title).toEqual('frontend-challenge');
  });


  it(`Check OnInit`, () => {
    const fixture = TestBed.createComponent(AppComponent);
    let ngOnInitFn = AppComponent.prototype.ngOnInit;
    AppComponent.prototype.ngOnInit = () => {} // override ngOnInit

    fixture.detectChanges();
    AppComponent.prototype.ngOnInit = ngOnInitFn; // revert ngOnInit
  });
});
