import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { NotFoundComponent } from 'src/app/shared';
import { MainComponent } from './pages/main/main.component';

const routes: Routes = [
  { path: 'home', component: MainComponent },
  { path: '', component: MainComponent },
  { path: '404', component: NotFoundComponent },
  { path: '**', redirectTo: '/404' }
];


@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class HomeRoutingModule { }
