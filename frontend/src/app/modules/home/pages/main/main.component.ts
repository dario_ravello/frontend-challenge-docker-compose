import { Component, OnInit } from '@angular/core';
import { BottlesState } from 'src/app/core/models/bottles-state.model';
import { WaterJugRiddleService } from 'src/app/core/services/water-jug-riddle.service';

@Component({
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  public bottleX: string = "2";
  public bottleY: string = "10";
  public bottleZ: string = "4";

  public error : string = "";
  public combinations : BottlesState[] = [];

  constructor(private waterJugRiddleService: WaterJugRiddleService) { }

  ngOnInit(): void { }


  onPressCalculate(bottleX: string, bottleY: string, bottleZ: string){
    this.waterJugRiddleService.validateBottles(Number(bottleX), Number(bottleY), Number(bottleZ));
    this.waterJugRiddleService.getWaterBottleRiddle(Number(bottleX), Number(bottleY), Number(bottleZ)).subscribe(data => {
      if(!data.length) {
        this.error = data.toString();
        return data.length;
      }
      this.combinations = data;
      return this.combinations;
    });
  }
}
