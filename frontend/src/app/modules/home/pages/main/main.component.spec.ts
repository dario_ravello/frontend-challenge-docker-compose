import { ComponentFixture, fakeAsync, TestBed, tick } from '@angular/core/testing';
import { NO_ERRORS_SCHEMA } from '@angular/core';
import { WaterJugRiddleService } from 'src/app/core/services/water-jug-riddle.service';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { MainComponent } from './main.component';
import { of } from 'rxjs';
import { BottlesState } from 'src/app/core/models/bottles-state.model';
import { delay } from 'rxjs/operators';

let component: MainComponent;
let fixture: ComponentFixture<MainComponent>;

describe('MainComponent', () => {

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      schemas: [NO_ERRORS_SCHEMA],
      declarations: [MainComponent],
      providers: [WaterJugRiddleService]
    });
    fixture = TestBed.createComponent(MainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('can load instance', () => {
    expect(component).toBeTruthy();
  });

  it(`bottleX has default value`, () => {
    expect(component.bottleX).toEqual(`2`);
  });

  it(`bottleY has default value`, () => {
    expect(component.bottleY).toEqual(`10`);
  });

  it(`bottleZ has default value`, () => {
    expect(component.bottleZ).toEqual(`4`);
  });

  it(`combinations has default value`, () => {
    expect(component.combinations).toEqual([]);
  });

  describe('onPressCalculate', () => {
    it('makes expected calls', () => {
      const waterJugRiddleServiceStub: WaterJugRiddleService = fixture.debugElement.injector.get(
        WaterJugRiddleService
      );
      spyOn(
        waterJugRiddleServiceStub,
        'getWaterBottleRiddle'
      ).and.callThrough();
      component.onPressCalculate("5","4","3");
      expect(
        waterJugRiddleServiceStub.getWaterBottleRiddle
      ).toHaveBeenCalled();
    });
  });

  describe('onPressCalculate 2', () => {
    it('makes expected calls', () => {
      const waterJugRiddleServiceStub: WaterJugRiddleService = fixture.debugElement.injector.get(
        WaterJugRiddleService
      );
      spyOn(
        waterJugRiddleServiceStub,
        'getWaterBottleRiddle'
      ).and.callThrough();
      component.onPressCalculate("12","4","3");
      expect(
        waterJugRiddleServiceStub.getWaterBottleRiddle
      ).toHaveBeenCalled();
    });
  });

  describe('onPressCalculate 3', () => {
    it('makes expected calls', () => {
      const waterJugRiddleServiceStub: WaterJugRiddleService = fixture.debugElement.injector.get(
        WaterJugRiddleService
      );
      spyOn(
        waterJugRiddleServiceStub,
        'getWaterBottleRiddle'
      ).and.callThrough();
      expect(component.onPressCalculate("1","7","11")).toBeFalsy();
    });

    it('On init users should be loaded', fakeAsync(() => {
      component.onPressCalculate("1","7","11")

      // Trigger ngOnInit()
      fixture.detectChanges();

      expect(component.combinations).toBeTruthy();
      expect(component.error).toEqual("");

      tick(1);
    }));
  });

});
