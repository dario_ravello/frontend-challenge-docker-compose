import { HttpClientModule } from '@angular/common/http';
import { TestBed } from '@angular/core/testing';
import { browser } from 'protractor';

import { LoadingService } from './loading.service';

describe('LoadingService', () => {
  let service: LoadingService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports:[ HttpClientModule ],
      providers: [ LoadingService ]
    });
    service = TestBed.inject(LoadingService);
  });

  it('should be created', () => {
    expect(service);
  });

  it('set loading without URL', () => {
    expect(service.setLoading(true, "false")).toContain('The request URL must be provided to the LoadingService.setLoading function');
  });

  it('set loading true', () => {
    service.setLoading(true, "http://localhost:4200");
  });

  it('set loading false', () => {
    service.loadingMap.set("http://localhost:4200", true);
    service.setLoading(false, "http://localhost:4200");
  });
});
