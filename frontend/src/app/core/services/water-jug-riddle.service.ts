import { HttpClient, HttpErrorResponse } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, of } from 'rxjs';
import { catchError, map } from 'rxjs/operators';
import { environment } from 'src/environments/environment';
import { BottlesState } from '../models/bottles-state.model';

@Injectable({
  providedIn: 'root'
})
export class WaterJugRiddleService {

  bottleXMax: number = 5;
  bottleYMax: number = 3;

  constructor(private http: HttpClient) { }

  getWaterBottleRiddle(bottleX: number, bottleY: number, bottleZ: number): Observable<BottlesState[]> {
    const url: string = environment.api.getWaterBottleRiddle.replace(":bottleX", bottleX.toString()).replace(":bottleY", bottleY.toString()).replace(":bottleZ", bottleZ.toString());
    return this.http.get<BottlesState[]>(url).pipe(catchError(this.handleError<BottlesState[]>('getWaterBottleRiddle', [])));
  }

  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {
      return of(result as T);
    };
  }

  validateBottles(bottleX: number, bottleY: number, bottleZ: number): string {
    // Check empty values
    if (!bottleX) {
      return "You need to fill in the quantity of the bottle X";
    }

    if (!bottleY) {
      return "You need to fill in the quantity of the bottle Y";
    }

    if (!bottleZ) {
      return "You need to fill in the quantity of the bottle Z";
    }

    // Validate jugs
    if (bottleX.toString().length <= 0 || bottleX.toString().length > 15) {
      return "The quantity of the bottle X must be between 0 and 999999999999999";
    }
    if (bottleY.toString().length <= 0 || bottleY.toString().length > 15) {
      return "The quantity of the bottle Y must be between 0 and 999999999999999";
    }
    if (bottleZ.toString().length <= 0 || bottleZ.toString().length > 15) {
      return "The quantity of the bottle Z must be between 0 and 999999999999999";
    }

    // Validate measure
    const maxValue = Math.max(bottleX, bottleY);
    if (bottleZ >= maxValue) {
      return "The quantity of the bottle z must be greater than those of X and Y"
    }

    // Validate if there is a solution
    const gcdNumber: number = this.gcd_two_numbers(bottleX, bottleY);
    const isMultiple: boolean = (bottleZ % gcdNumber) === 0 ;

    if (!gcdNumber || !isMultiple) {
      return "No Solution"
    }

    // Return valid
    return "";
  }

  gcd_two_numbers(x: number, y: number) {
    x = Math.abs(x);
    y = Math.abs(y);
    while (y) {
      var t = y;
      y = x % y;
      x = t;
    }
    return x;
  }
}
