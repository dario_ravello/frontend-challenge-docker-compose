
import { TestBed } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { WaterJugRiddleService } from './water-jug-riddle.service';

describe('WaterJugRiddleService', () => {
  let service: WaterJugRiddleService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [WaterJugRiddleService]
    });
    service = TestBed.inject(WaterJugRiddleService);
  });

  it('can load instance', () => {
    expect(service).toBeTruthy();
  });

  it(`bottleXMax has default value`, () => {
    expect(service.bottleXMax).toEqual(5);
  });

  it(`bottleYMax has default value`, () => {
    expect(service.bottleYMax).toEqual(3);
  });

  it(`check calculateWaterJugRiddle`, () => {
    service.getWaterBottleRiddle(5,3,4).subscribe(result => expect(result.length).toBeGreaterThan(0));
  });

  it(`check calculateWaterJugRiddle Error 1`, () => {
    service.getWaterBottleRiddle(0,3,4).subscribe(result => expect(result.length).toEqual(0));
  });

  it(`check calculateWaterJugRiddle Error 2`, () => {
    service.getWaterBottleRiddle(5,0,4).subscribe(result => expect(result.length).toEqual(0));
  });

  it(`check calculateWaterJugRiddle Error 3`, () => {
    service.getWaterBottleRiddle(5,3,0).subscribe(result => expect(result.length).toEqual(0));
  });

  it(`check calculateWaterJugRiddle Error 7`, () => {
    service.getWaterBottleRiddle(1234,3,4).subscribe(result => expect(result.length).toEqual(0));
  });

  it(`check calculateWaterJugRiddle Error 8`, () => {
    service.getWaterBottleRiddle(5,123,4).subscribe(result => expect(result.length).toEqual(0));
  });

  it(`check calculateWaterJugRiddle Error 9`, () => {
    service.getWaterBottleRiddle(5,3,123).subscribe(result => expect(result.length).toEqual(0));
  });

  it(`check calculateWaterJugRiddle Error 10`, () => {
    service.getWaterBottleRiddle(1,3,5).subscribe(result => expect(result.length).toEqual(0));
  });

  it(`check validateBottles Error 1`, () => {
    expect(service.validateBottles(0,3,5)).toEqual("You need to fill in the quantity of the bottle X");
  });
  it(`check validateBottles Error 2`, () => {
    expect(service.validateBottles(1,0,5)).toEqual("You need to fill in the quantity of the bottle Y");
  });
  it(`check validateBottles Error 3`, () => {
    expect(service.validateBottles(1,3,0)).toEqual("You need to fill in the quantity of the bottle Z");
  });
  it(`check validateBottles Error 4`, () => {
    expect(service.validateBottles(11111111111111111,3,5)).toEqual("The quantity of the bottle X must be between 0 and 999999999999999");
  });
  it(`check validateBottles Error 5`, () => {
    expect(service.validateBottles(2,11111111111111111,5)).toEqual("The quantity of the bottle Y must be between 0 and 999999999999999");
  });
  it(`check validateBottles Error 6`, () => {
    expect(service.validateBottles(2,3,11111111111111111)).toEqual("The quantity of the bottle Z must be between 0 and 999999999999999");
  });

});
