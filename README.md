# water-jug-riddle

## Prerequisites
- [Node](https://nodejs.org/)
- [AngularCLI](https://angular.io/cli)
- [Docker](https://docs.docker.com/docker-for-windows/install/)

## Run docker compose
In your terminal in the root folder

```sh
docker-compose up
```
And open your browser at [http://localhost:4200/](http://localhost:4200/)

## Read other readme files
- [Frontend Readme](frontend/README.md)
- [Backend Readme](backend/README.md)