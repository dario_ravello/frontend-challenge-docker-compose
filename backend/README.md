# Backend

## Prerequisites
- [.NET Core 2.1](https://dotnet.microsoft.com/download/dotnet/2.1)

## Build backend
In your terminal in the backend folder

```sh
cd backend
dotnet publish
```

## Debug mode
If you want to debug the code you can use a C # IDE, run the code and consult it in the following link

```bash
https://localhost:5001/api/waterbottle/2/10/4
```

## Code Arquitecture
This is the project structure. This is the arquitecutre of the backend.

```bash
src/
|
|– bin/                                     # Genereted compilation files
|– Controllers/                             # Controllers folder
|– Models/                                  # Models folder
|– Properties/                              # Properties folder
|– Utils/                                   # Utils folder
– appsettings.json                          # Settings file
– Program.cs                                # Main file
– Program.cs                                # Main file
– Startup.cs                                # Startup file
– server.csproj                             # Project file
```

## TODO List
- Create the unit testing whit 100% of coverage
- Create the overflow control of apis
- Improve the strategie of backend
- Automate docker compose whit sh files
- Fix bug: State bottle description
