namespace backend.Utils
{
    class Constants
    {
        public const int BOTTLE_STATE_EMPTY = 0;
        public const string BOTTLE_STATE_EMPTY_DESCRIPTION = "Empty";
        public const string BOTTLE_STATE_FULL_DESCRIPTION = "Full";
        public const string BOTTLE_STATE_PARTIAL_FULL_DESCRIPTION = "Partial Full";
        public const string BOTTLE_STATE_ERROR_DESCRIPTION = "Error";

        public const string ERROR_MESSAGE_TOO_BIG_RESPONSE = "The result cannot be displayed, there are more than 250 combinations for these values";
        public const string ERROR_VALIDATION_MESSAGE_X_BOTTLE_WRONG_VALUE = "The quantity of the bottle X must be between 0 and 999999999999999";
        public const string ERROR_VALIDATION_MESSAGE_Y_BOTTLE_WRONG_VALUE = "The quantity of the bottle Y must be between 0 and 999999999999999";
        public const string ERROR_VALIDATION_MESSAGE_Z_BOTTLE_WRONG_VALUE = "The quantity of the bottle Z must be between 0 and 999999999999999";
        public const string ERROR_VALIDATION_MESSAGE_Z_BOTTLE_BIGGER_VALUE = "The quantity of the bottle z must be greater than those of X and Y";
        public const string ERROR_VALIDATION_MESSAGE_NO_SOLUTION = "No Solution";
    }
}