﻿using System;
using System.Collections.Generic;
using backend.Utils;
using Newtonsoft.Json;
using server.Models;

namespace server.Utils
{
    public class WaterBottleUtil
    {
        private static int MAX_BOTTLE_X = 0;
        private static int MAX_BOTTLE_Y = 0;
        public static List<Pair> minSteps(int to, int from, int d)
        {
            MAX_BOTTLE_X = to;
            MAX_BOTTLE_Y = from;
            
            if(to > from){
                int temp = from;
                from = to;
                to = temp;
            }

            if(d>from){
                return new List<Pair>();
            }
            if ((d % gcd(from,to)) != 0){
                return new List<Pair>();
            }

            List<Pair> list1 = pour(to,from,d);
            List<Pair> list2 = pour(from,to,d);
            if(list1.Count < list2.Count)
                return list1;
            else
                return list2;
        }
        
        private static int gcd(int a, int b){
            if(a==0)
                return b;
            return gcd(b%a, a);
        }
        
        private static string getBottleState(int quantity, int total){
            if(quantity==0)
                return Constants.BOTTLE_STATE_EMPTY_DESCRIPTION;

            if (quantity == total)
                return Constants.BOTTLE_STATE_FULL_DESCRIPTION;

            if (quantity < total)
                return Constants.BOTTLE_STATE_PARTIAL_FULL_DESCRIPTION;

            return Constants.BOTTLE_STATE_ERROR_DESCRIPTION;
        }

        private static List<Pair> pour(int fromCap, int toCap, int d){
            List<Pair> list = new List<Pair>();
            int from = fromCap;
            int to = 0;
            list.Add(new Pair(to, from, getBottleState(to, MAX_BOTTLE_X), getBottleState(from, MAX_BOTTLE_Y)));

            while (from != d && to!=d){
                int temp = Math.Min(from, toCap-to);
                to += temp;
                from -= temp;
                list.Add(new Pair(to,from, getBottleState(to, MAX_BOTTLE_X), getBottleState(from, MAX_BOTTLE_Y)));

                if(from == d || to == d)
                    break;
                if (from == 0)
                {
                    from = fromCap;
                    list.Add(new Pair(to,from, getBottleState(to, MAX_BOTTLE_X), getBottleState(from, MAX_BOTTLE_Y)));
                }

                // If second jug becomes full, empty it
                if (to == toCap)
                {
                    to = 0;
                    list.Add(new Pair(to,from, getBottleState(to, MAX_BOTTLE_X), getBottleState(from, MAX_BOTTLE_Y)));
                }
            }
            return list;
        }
        
        
        public static string validateBottles(int bottleX, int bottleY, int bottleZ) {
            // Validate bottles
            if (bottleX.ToString().Length <= 0 || bottleX.ToString().Length > 15) {
                return Constants.ERROR_VALIDATION_MESSAGE_X_BOTTLE_WRONG_VALUE;
            }
            if (bottleY.ToString().Length <= 0 || bottleY.ToString().Length > 15) {
                return Constants.ERROR_VALIDATION_MESSAGE_Y_BOTTLE_WRONG_VALUE;
            }
            if (bottleZ.ToString().Length <= 0 || bottleZ.ToString().Length > 15) {
                return Constants.ERROR_VALIDATION_MESSAGE_Z_BOTTLE_WRONG_VALUE;
            }

            // Validate measure
            int maxValue = Math.Max(bottleX, bottleY);
            if (bottleZ >= maxValue)
            {
                return Constants.ERROR_VALIDATION_MESSAGE_Z_BOTTLE_BIGGER_VALUE;
            }

            // Validate if there is a solution
            int gcdNumber = gcd(bottleX, bottleY);
            Boolean isMultiple = (bottleZ % gcdNumber) == 0;

            if (gcdNumber == 0 || !isMultiple)
            {
                return Constants.ERROR_VALIDATION_MESSAGE_NO_SOLUTION;
            }

            // Return valid
            return "";
        }
    }
}