﻿using backend.Utils;

namespace server.Models
{
    public class BottleState
    {
        public int bottleX { get; set; }
        public string bottleXState { get; set; }
        public int bottleY { get; set; }
        public string bottleYState { get; set; }

        public BottleState(int bottleX, string bottleXState, int bottleY, string bottleYState)
        {
            this.bottleX = bottleX;
            this.bottleXState = bottleXState;
            this.bottleY = bottleY;
            this.bottleYState = bottleYState;
        }

        public BottleState()
        {
            bottleX = Constants.BOTTLE_STATE_EMPTY;
            bottleXState = Constants.BOTTLE_STATE_EMPTY_DESCRIPTION;
            bottleY = Constants.BOTTLE_STATE_EMPTY;
            bottleYState = Constants.BOTTLE_STATE_EMPTY_DESCRIPTION;
        }
    }
}