﻿using System;

namespace server.Models
{
    public class Pair
    { 
        public int bottleX { get; set; }
        public int bottleY { get; set; }
        public string bottleXState { get; set; }
        public string bottleYState { get; set; }
        
        public Pair(int bottleX, int bottleY, string bottleXState, string bottleYState) {
            this.bottleX = bottleX;
            this.bottleY = bottleY;
            this.bottleXState = bottleXState;
            this.bottleYState = bottleYState;
        }
    }
}