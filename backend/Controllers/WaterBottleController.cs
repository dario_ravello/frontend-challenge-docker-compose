﻿using System;
using System.Collections.Generic;
using backend.Utils;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using server.Models;
using server.Utils;

namespace server.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class WaterBottleController : ControllerBase
    {
        // GET api/waterbottle/bottleX/bottleY/bottleZ
        [HttpGet("{bottleX}/{bottleY}/{bottleZ}")]
        public ActionResult<string> Get(int bottleX,int bottleY,int bottleZ)
        {
            string validationMessage = WaterBottleUtil.validateBottles(bottleX, bottleY, bottleZ);

            if (!String.IsNullOrEmpty(validationMessage)) {
                return validationMessage;
            }
            
            List<Pair> path = WaterBottleUtil.minSteps(bottleX, bottleY, bottleZ);
            if (path == null)
                return null;

            if (path.Count > 250) 
                return Constants.ERROR_MESSAGE_TOO_BIG_RESPONSE; 
            return JsonConvert.SerializeObject(path);
        }
    }
}
